/**
 * Copyright (c) 2007-2012 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * @author Ariel Flesler
 * @version 1.4.3.1
 */
;(function($){var h=$.scrollTo=function(a,b,c){$(window).scrollTo(a,b,c)};h.defaults={axis:'xy',duration:parseFloat($.fn.jquery)>=1.3?0:1,limit:true};h.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(e,f,g){if(typeof f=='object'){g=f;f=0}if(typeof g=='function')g={onAfter:g};if(e=='max')e=9e9;g=$.extend({},h.defaults,g);f=f||g.duration;g.queue=g.queue&&g.axis.length>1;if(g.queue)f/=2;g.offset=both(g.offset);g.over=both(g.over);return this._scrollable().each(function(){if(e==null)return;var d=this,$elem=$(d),targ=e,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}$.each(g.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=h.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(g.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=g.offset[pos]||0;if(g.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*g.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(g.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&g.queue){if(old!=attr[key])animate(g.onAfterFirst);delete attr[key]}});animate(g.onAfter);function animate(a){$elem.animate(attr,f,g.easing,a&&function(){a.call(this,e,g)})}}).end()};h.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);

$(function() {

    window.last_scroll_position = 0;

    var show = false

    var to_top_button = $('<div class="to_top" ><div class="to_top_panel" ><div class="to_top_button" title="Наверх"><span class="arrow">&uarr;</span> <span class="label">наверх</span></div></div></div>')

    $('body').append(to_top_button);

    // наверх
    $('.to_top_panel', to_top_button).click(function(){
        if(to_top_button.hasClass('has_position')){
            to_top_button.removeClass('has_position');
            $('.to_top_button .arrow', to_top_button).html('&uarr;');
            $('.to_top_button .label', to_top_button).html('наверх');
            $.scrollTo( window.last_scroll_position , 100,  { axis: 'y' } );
            window.last_scroll_position = 0;
        }else{
            to_top_button.addClass('has_position');
            $('.to_top_button .arrow', to_top_button).html('&darr;');
            $('.to_top_button .label', to_top_button).html('вниз');
            window.last_scroll_position = window.pageYOffset;
            $.scrollTo( $('body') , 100,  { axis: 'y' } );
        }
    })


    var last_position = 0;

    $(window).scroll(function () {
        show_or_hide()
        if( last_position < window.pageYOffset){
            //console.log('скролл вниз', last_position , window.pageYOffset);
            if( to_top_button.hasClass('has_position') ){
                //to_top_button.removeClass('has_position');
                //$('.to_top_button .arrow', to_top_button).html('&uarr;');
                //$('.to_top_button .label', to_top_button).html('наверх');
                //to_top_button.hide()
                show = false
            }
        }else{
            //console.log('скролл вверх', last_position , window.pageYOffset);
        }
        last_position = window.pageYOffset;
    })


    function show_or_hide(){
        if( window.pageYOffset > 400){
            if(!show){
                to_top_button.show()
                to_top_button.removeClass('has_position');
                $('.to_top_button .arrow', to_top_button).html('&uarr;');
                $('.to_top_button .label', to_top_button).html('наверх');
                show = true
            }
        }else{
            if(show && !to_top_button.hasClass('has_position')){
                to_top_button.hide()
                show = false
            }
        }
    }


    show_or_hide()

});