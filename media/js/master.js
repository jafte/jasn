jQuery.fn.extend({
    insertAtCaret: function(myValue){
      return this.each(function(i) {
        if (document.selection) {
          //For browsers like Internet Explorer
          this.focus();
          sel = document.selection.createRange();
          sel.text = myValue;
          this.focus();
        }
        else if (this.selectionStart || this.selectionStart == '0') {
          //For browsers like Firefox and Webkit based
          var startPos = this.selectionStart;
          var endPos = this.selectionEnd;
          var scrollTop = this.scrollTop;
          this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
          this.focus();
          this.selectionStart = startPos + myValue.length;
          this.selectionEnd = startPos + myValue.length;
          this.scrollTop = scrollTop;
        } else {
          this.value += myValue;
          this.focus();
        }
      })
    }
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    crossDomain: false, // obviates need for sameOrigin test
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function sendAjax(btn, url) {
    if (!btn.hasClass("disabled")) {
        btn.addClass("disabled");
        group = btn.parent();
        id = btn.attr('data-id');
        score = btn.attr('data-value');
        value = $('b', group.parent());

        $.ajax({
            url: url + id +'/'+ score +'/',
            success: function(data, textStatus) {
                if (data == "Vote recorded.") {
                    $('a', group).remove();
                    value_text = parseFloat(value.text());
                    value_text = value_text + parseFloat(score);
                    value.text(value_text);
                }
            },
            error: function(){
                btn.removeClass("disabled");
            }
        });
    };
};

$('.comment .vote a').on('click', function(event){
    event.preventDefault();
    btn = $(this);
    sendAjax(btn, '/api/ratecomment/');
});

$('.post-vote.vote a').on('click', function(event){
    event.preventDefault();
    btn = $(this);
    sendAjax(btn, '/api/ratepost/');
});

$('.wall .vote a').on('click', function(event){
    event.preventDefault();
    btn = $(this);
    sendAjax(btn, '/api/ratewall/');
});

$(".edit-url").on('click', function(event){
    event.preventDefault();
    ahref = $(this);
    comment = ahref.parents('.item');
    url = ahref.attr("data-url");
    $.getJSON(url, function(data){
        if (data.status == 'error') {
            alert(data.message);
        } else {
            $(".commentbody", comment).html("<textarea class='comment-edit' name='comment'>" + data.message + "</textarea><a href='#' class='btn save-comment' data-url='" + url + "'>сохранить</a>")
            $(".edit", comment).hide();
            $(".save-comment").off('click');
            $(".save-comment").on('click', function(event){
                event.preventDefault();
                ahref = $(this);
                comment = ahref.parents('.item');
                url = ahref.attr("data-url");
                $.ajax({
                    'url': url,
                    'type': "POST",
                    'data': {
                        'comment': $(".comment-edit", comment).val()
                    },
                    'dataType': 'json'
                }).done(function ( data ) {
                        comment = $('.item#c'+data['id']);
                        if (data['status'] == 'error') {
                            alert(data['message']);
                        } else {
                            $(".commentbody", comment).html(data['message']);
                            $(".edit", comment).show();
                            $('span.splr').before('<a class="togglesplr" href="#">открыть споилер</a>');

                            $('span.splr').before('<a class="togglesplr" href="#">открыть споилер</a>');
                            $('.togglesplr').off('click');
                            $('.togglesplr').on('click', function(){
                                self = $(this);
                                item = self.next();
                                item.show();
                                self.remove();
                                return false;
                            });
                        };
                    });
            });
        }
    });
});


$(".delete-url").on('click', function(event){
    event.preventDefault();
    ahref = $(this);
    comment = ahref.parents('.item');
    url = ahref.attr("data-url");
    $.getJSON(url, {'comment': $(".comment-edit", comment).val()}, function(data){
        if (data.status == 'error') {
            alert(data.message);
        } else {
            comment.html("<div class='alert'>Комменатрий удален</div>");
        }
    });
});

$('span.splr').before('<a class="togglesplr" href="#">открыть споилер</a>');
$('.togglesplr').on('click', function(){
    self = $(this);
    item = self.next();
    item.show();
    self.remove();
    return false;
});

$('.comment-submit')
      .click(function () {
        var btn = $(this);
        btn.button('loading');
      })

$(document).ready(function(){
 // I added the video size here in case you wanted to modify it more easily
 var vidWidth = 425;
 var vidHeight = 344;

 var obj = '<object width="' + vidWidth + '" height="' + vidHeight + '">' +
     '<param name="movie" value="http://www.youtube.com/v/[vid]&hl=en&fs=1">' +
     '</param><param name="allowFullScreen" value="true"></param><param ' +
     'name="allowscriptaccess" value="always"></param><em' +
     'bed src="http://www.youtube.com/v/[vid]&hl=en&fs=1" ' +
     'type="application/x-shockwave-flash" allowscriptaccess="always" ' +
     'allowfullscreen="true" width="' + vidWidth + '" ' + 'height="' +
     vidHeight + '"></embed></object> ';

 $('.commentbody:contains("youtube.com/watch")').each(function(){
  var that = $(this);
  var vid = that.html().match(/(?:v=)([\w\-]+)/g); // end up with v=oHg5SJYRHA0
  if (vid.length) {
   $.each(vid, function(){
    that.append( obj.replace(/\[vid\]/g, this.replace('v=','')) );
   });
  }
 });

  $("a[href$='.jpg'],a[href$='.png'],a[href$='.gif']").attr('rel', 'gallery').fancybox({
    prevEffect    : 'none',
    nextEffect    : 'none',
    closeBtn    : false,
    arrows      : false,
    helpers   : {
      title : { type : 'inside' },
      buttons : {}
    }
  });

});
