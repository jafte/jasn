from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.contrib import admin
from djangoratings.views import AddRatingFromModel
from views import CommentListView, WallListView

from django.contrib.sitemaps import FlatPageSitemap, GenericSitemap

from blog.models import Post, Blog, Category
from user_groups.models import Group
from profiles.models import Profile

post_dict = {
    'queryset': Post.objects.published(),
    'date_field': 'publish',
    }

blog_dict = {
    'queryset': Blog.objects.filter(is_delete = False),
    'date_field': 'modified',
    }

club_dict = {
    'queryset': Group.objects.filter(is_active = True),
    'date_field': 'modified',
    }

cat_dict = {
    'queryset': Category.objects.all(),
    }

user_dict = {
    'queryset': Profile.objects.filter(user__is_active = True),
    }


sitemaps = {
    'post': GenericSitemap(post_dict, priority=0.6),
    'blog': GenericSitemap(blog_dict, priority=0.5),
    'cat': GenericSitemap(cat_dict, priority=0.6),
    'club': GenericSitemap(club_dict, priority=0.7),
    'user': GenericSitemap(user_dict, priority=0.7),
    }

admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', 'views.index'),

    url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    
    url(r'^barwiki/', include('wakawaka.urls')),

    url(r'^blogs/', include('blog.urls')),
    url(r'^clubs/', include('user_groups.urls')),

    url(r'^comment/posted/$', 'blog.views.comment_posted' ),
    url(r'^comment/', include('django.contrib.comments.urls')),

    url(r'^wall/', include('wall.urls')),
	
	url(r'^search/', include('haystack.urls')),

    url(r'^u/', include('userena.urls')),
    url(r'^msg/', include('userena.contrib.umessages.urls')),
    url(r'^relation/', include('relationships.urls')),


    url(r'^admin/', include(admin.site.urls)),

    url(r'^invite/', include('invitations.urls'), ),

    url(r'^comments/$', CommentListView.as_view(), name='comments_all'),
    url(r'^comments/page-(?P<page>\d+)/$', CommentListView.as_view(), name='comments_all_paginated'),

    url(r'^walls/$', WallListView.as_view(), name='wall_items_all'),
    url(r'^walls/page-(?P<page>\d+)/$', WallListView.as_view(), name='wall_items_all_paginated'),

)

urlpatterns += patterns('',
    url(r'api/ratecomment/(?P<object_id>\d+)/(?P<score>[-\d]+)/', AddRatingFromModel(), {
        'app_label': 'threadedcomments',
        'model': 'threadedcomment',
        'field_name': 'rating',
        }),
    url(r'api/ratewall/(?P<object_id>\d+)/(?P<score>[-\d]+)/', AddRatingFromModel(), {
        'app_label': 'wall',
        'model': 'wallitem',
        'field_name': 'rating',
        }),
    url(r'api/ratepost/(?P<object_id>\d+)/(?P<score>[-\d]+)/', AddRatingFromModel(), {
        'app_label': 'blog',
        'model': 'post',
        'field_name': 'rating',
        }),
    url(r'api/favepost/(?P<object_id>\d+)/(?P<score>[-\d]+)/', AddRatingFromModel(), {
        'app_label': 'blog',
        'model': 'post',
        'field_name': 'favorite',
        }),
)


if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^m/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
    )