# coding=utf-8
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.core.urlresolvers import reverse
from django.views.generic.simple import direct_to_template
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.models import User
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.utils.translation import ungettext
from django.contrib.auth import REDIRECT_FIELD_NAME

from userena.contrib.umessages.models import Message, MessageRecipient, MessageContact
from userena.contrib.umessages.forms import ComposeForm
from userena.utils import get_datetime_now
from userena import settings as userena_settings

from pure_pagination.mixins import PaginationMixin
from django.views.generic import ListView
from django.utils.decorators import method_decorator

class MessageListListView(PaginationMixin, ListView):
    model = MessageContact
    context_object_name = 'message_list'
    template_name = 'umessages/message_list.html'
    paginate_by = 20

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(MessageListListView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = MessageContact.objects.get_contacts_for(self.request.user)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(MessageListListView, self).get_context_data(**kwargs)
        self.request.breadcrumbs(u'Сообщения', '#')

        return context

class MessageListView(PaginationMixin, ListView):
    model = Message
    context_object_name = 'message_list'
    template_name = 'umessages/message_detail.html'
    paginate_by = 20

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(MessageListView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        recipient = get_object_or_404(User, username__iexact=self.kwargs.get('username', False))
        queryset = Message.objects.get_conversation_between(self.request.user, recipient)

        # Update all the messages that are unread.
        message_pks = [m.pk for m in queryset]
        unread_list = MessageRecipient.objects.filter(
            message__in=message_pks,
            user=self.request.user,
            read_at__isnull=True
        )
        now = get_datetime_now()
        unread_list.update(read_at=now)

        return queryset

    def get_context_data(self, **kwargs):
        recipient = get_object_or_404(User, username__iexact=self.kwargs.get('username', False))
        context = super(MessageListView, self).get_context_data(**kwargs)
        context['recipient'] = recipient

        self.request.breadcrumbs(u'Сообщения', reverse('userena_umessages_list'))
        self.request.breadcrumbs( (u'Переписка с %s' % recipient.profile.get_full_name_or_username()), '#')

        return context


@login_required
def message_compose(request, recipients=None, compose_form=ComposeForm,
                    success_url=None, template_name="umessages/message_form.html",
                    recipient_filter=None, extra_context=None):
    initial_data = dict()

    if recipients:
        username_list = [r.strip() for r in recipients.split("+")]
        recipients = [u for u in User.objects.filter(username__in=username_list)]
        initial_data["to"] = recipients

    form = compose_form(initial=initial_data)
    if request.method == "POST":
        form = compose_form(request.POST)
        if form.is_valid():
            requested_redirect = request.REQUEST.get("next", False)

            message = form.save(request.user)
            recipients = form.cleaned_data['to']

            if userena_settings.USERENA_USE_MESSAGES:
                messages.success(request, _('Message is sent.'),
                                 fail_silently=True)

            requested_redirect = request.REQUEST.get(REDIRECT_FIELD_NAME,
                                                     False)

            # Redirect mechanism
            redirect_to = reverse('userena_umessages_list')
            if requested_redirect: redirect_to = requested_redirect
            elif success_url: redirect_to = success_url
            elif len(recipients) == 1:
                redirect_to = reverse('userena_umessages_detail',
                                      kwargs={'username': recipients[0].username})
            return redirect(redirect_to)

    if not extra_context: extra_context = dict()
    extra_context["form"] = form
    extra_context["recipients"] = recipients
    return direct_to_template(request,
                              template_name,
                              extra_context=extra_context)

@login_required
@require_http_methods(["POST"])
def message_remove(request, undo=False):

    message_pks = request.POST.getlist('message_pks')
    redirect_to = request.REQUEST.get('next', False)

    if message_pks:
        # Check that all values are integers.
        valid_message_pk_list = set()
        for pk in message_pks:
            try: valid_pk = int(pk)
            except (TypeError, ValueError): pass
            else:
                valid_message_pk_list.add(valid_pk)

        # Delete all the messages, if they belong to the user.
        now = get_datetime_now()
        changed_message_list = set()
        for pk in valid_message_pk_list:
            message = get_object_or_404(Message, pk=pk)

            # Check if the user is the owner
            if message.sender == request.user:
                if undo:
                    message.sender_deleted_at = None
                else:
                    message.sender_deleted_at = now
                message.save()
                changed_message_list.add(message.pk)

            # Check if the user is a recipient of the message
            if request.user in message.recipients.all():
                mr = message.messagerecipient_set.get(user=request.user,
                                                      message=message)
                if undo:
                    mr.deleted_at = None
                else:
                    mr.deleted_at = now
                mr.save()
                changed_message_list.add(message.pk)

        # Send messages
        if (len(changed_message_list) > 0) and userena_settings.USERENA_USE_MESSAGES:
            if undo:
                message = ungettext('Message is succesfully restored.',
                                    'Messages are succesfully restored.',
                                    len(changed_message_list))
            else:
                message = ungettext('Message is successfully removed.',
                                    'Messages are successfully removed.',
                                    len(changed_message_list))

            messages.success(request, message, fail_silently=True)

    if redirect_to: return redirect(redirect_to)
    else: return redirect(reverse('userena_umessages_list'))
