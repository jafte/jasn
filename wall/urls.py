from django.conf.urls import patterns, url

urlpatterns = patterns('wall.views',
    url(r'^post/$',          'wall_comment',       name='wall-post-comment'),
    url(r'^w/edit/(?P<comment_id>[\d]+)/$',
        'edit_item',
        name='wall_edit_comment'
    ),
    url(r'^w/del/(?P<comment_id>[\d]+)/$',
        'del_item',
        name='wall_del_comment'
    ),
)