import datetime, markdown
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes import generic
from djangoratings.fields import VotingField

class WallItem(models.Model):
    content_type = models.ForeignKey(ContentType,
        verbose_name=_('content type'),
        related_name="content_type_set_for_%(class)s")
    object_pk = models.TextField(_('object ID'))
    content_object = generic.GenericForeignKey(ct_field="content_type", fk_field="object_pk")

    # Metadata about the comment
    site = models.ForeignKey(Site)

    user = models.ForeignKey(User, verbose_name=_('user'),
        blank=True, null=True, related_name="%(class)s_wallitems")
    submit_date = models.DateTimeField(_('date/time submitted'), default=None)
    is_removed = models.BooleanField(_('is removed'), default=False,
        help_text=_('Check this box if the comment is inappropriate. '\
                    'A "This comment has been removed" message will '\
                    'be displayed instead.'))

    body = models.TextField(_('body'))
    body_html = models.TextField(_('body'))

    rating = VotingField()

    def save(self, *args, **kwargs):
        html = markdown.markdown(self.body, extensions=['nl2br', ])
        self.body_html = html

        super(WallItem, self).save(*args, **kwargs)


    class Meta:
        permissions = [("can_moderate", "Can moderate wall")]
        verbose_name = _('wall')
        verbose_name_plural = _('walls')
        ordering = ['-submit_date',]

    def get_editable(self):
        week = datetime.datetime.now() - datetime.timedelta(days=7)
        if self.submit_date >= week:
            return True
        else:
            return False