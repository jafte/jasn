from django.contrib import admin
from wall.models import WallItem

class ProfileWallItem(admin.ModelAdmin):
    list_display  = ('user', 'submit_date', 'is_removed')
    search_fields = ('body', 'body_html')

admin.site.register(WallItem, ProfileWallItem)
