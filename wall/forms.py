from django.utils import timezone
from django import forms
from django.contrib.comments.forms import CommentSecurityForm
from django.contrib.contenttypes.models import ContentType
from django.utils.encoding import force_unicode
import settings
from wall.models import WallItem
from django.utils.translation import ugettext_lazy as _

class AddWallItem(CommentSecurityForm):

    def __init__(self, target_object, parent=None, data=None, initial=None):
        if initial is None:
            initial = {}
        super(AddWallItem, self).__init__(target_object, data=data,
            initial=initial)

    comment       = forms.CharField(label=_('Comment'), widget=forms.Textarea)

    honeypot      = forms.CharField(required=False,
        label=_('If you enter anything in this field '\
                'your comment will be treated as spam'))

    def clean_honeypot(self):
        value = self.cleaned_data["honeypot"]
        if value:
            raise forms.ValidationError(self.fields["honeypot"].label)
        return value

    def get_comment_object(self):
        """
        Return a new (unsaved) comment object based on the information in this
        form. Assumes that the form is already validated and will throw a
        ValueError if not.

        Does not set any of the fields that would come from a Request object
        (i.e. ``user`` or ``ip_address``).
        """
        if not self.is_valid():
            raise ValueError("get_comment_object may only be called on valid forms")

        CommentModel = self.get_comment_model()
        new = CommentModel(**self.get_comment_create_data())
        new = self.check_for_duplicate_comment(new)

        return new

    def get_comment_create_data(self):
        """
        Returns the dict of data to be used to create a comment. Subclasses in
        custom comment apps that override get_comment_model can override this
        method to add extra fields onto a custom comment model.
        """
        return dict(
            content_type = ContentType.objects.get_for_model(self.target_object),
            object_pk    = force_unicode(self.target_object._get_pk_val()),
            body         = self.cleaned_data["comment"],
            submit_date  = timezone.now(),
            site_id      = settings.SITE_ID,
        )

    def check_for_duplicate_comment(self, new):
        """
        Check that a submitted comment isn't a duplicate. This might be caused
        by someone posting a comment twice. If it is a dup, silently return the *previous* comment.
        """
        possible_duplicates = self.get_comment_model()._default_manager.using(
            self.target_object._state.db
        ).filter(
            content_type = new.content_type,
            object_pk = new.object_pk,
        )
        for old in possible_duplicates:
            if old.submit_date.date() == new.submit_date.date() and old.body == new.body:
                return old

        return new

    def get_comment_model(self):
        return WallItem