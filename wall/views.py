from __future__ import absolute_import
from django.http import Http404, HttpResponse
from django.contrib.auth.decorators import login_required

from django import http
from django.conf import settings
import json, markdown
import wall
from django.contrib.comments import signals
from django.contrib.comments.views.utils import next_redirect, confirmation_view
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import models
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.html import escape
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST
from wall.templatetags.wall_tags import WallItem


class WallPostBadRequest(http.HttpResponseBadRequest):
    """
    Response returned when a comment post is invalid. If ``DEBUG`` is on a
    nice-ish error message will be displayed (for debugging purposes), but in
    production mode a simple opaque 400 page will be displayed.
    """
    def __init__(self, why):
        super(WallPostBadRequest, self).__init__()
        if settings.DEBUG:
            self.content = render_to_string("comments/400-debug.html", {"why": why})

@csrf_protect
@require_POST
def wall_comment(request, next=None, using=None):
    """
    Post a comment.

    HTTP POST is required. If ``POST['submit'] == "preview"`` or if there are
    errors a preview template, ``comments/preview.html``, will be rendered.
    """
    # Fill out some initial data fields from an authenticated user, if present
    data = request.POST.copy()
    if request.user.is_authenticated():
        if not data.get('name', ''):
            data["name"] = request.user.get_full_name() or request.user.username
        if not data.get('email', ''):
            data["email"] = request.user.email

    # Look up the object we're trying to comment about
    ctype = data.get("content_type")
    object_pk = data.get("object_pk")
    if ctype is None or object_pk is None:
        return WallPostBadRequest("Missing content_type or object_pk field.")
    try:
        model = models.get_model(*ctype.split(".", 1))
        target = model._default_manager.using(using).get(pk=object_pk)
    except TypeError:
        return WallPostBadRequest(
            "Invalid content_type value: %r" % escape(ctype))
    except AttributeError:
        return WallPostBadRequest(
            "The given content-type %r does not resolve to a valid model." %\
            escape(ctype))
    except ObjectDoesNotExist:
        return WallPostBadRequest(
            "No object matching content-type %r and object PK %r exists." %\
            (escape(ctype), escape(object_pk)))
    except (ValueError, ValidationError), e:
        return WallPostBadRequest(
            "Attempting go get content-type %r and object PK %r exists raised %s" %\
            (escape(ctype), escape(object_pk), e.__class__.__name__))

    # Do we want to preview the comment?
    preview = "preview" in data

    # Construct the comment form
    form = wall.get_form()(target, data=data)

    # Check security information
    if form.security_errors():
        return WallPostBadRequest(
            "The comment form failed security verification: %s" %\
            escape(str(form.security_errors())))

    # If there are errors or if we requested a preview show the comment
    if form.errors or preview:
        template_list = [
            # These first two exist for purely historical reasons.
            # Django v1.0 and v1.1 allowed the underscore format for
            # preview templates, so we have to preserve that format.
            "wall/%s_%s_preview.html" % (model._meta.app_label, model._meta.module_name),
            "wall/%s_preview.html" % model._meta.app_label,
            # Now the usual directory based template hierarchy.
            "wall/%s/%s/preview.html" % (model._meta.app_label, model._meta.module_name),
            "wall/%s/preview.html" % model._meta.app_label,
            "wall/preview.html",
            ]
        return render_to_response(
            template_list, {
                "comment": form.data.get("comment", ""),
                "form": form,
                "next": data.get("next", next),
                },
            RequestContext(request, {})
        )

    # Otherwise create the comment
    comment = form.get_comment_object()
    comment.ip_address = request.META.get("REMOTE_ADDR", None)
    if request.user.is_authenticated():
        comment.user = request.user

    # Save the comment and signal that it was saved
    comment.save()

    return next_redirect(request, next, wall_comment, w=comment._get_pk_val())

@login_required
def edit_item(request, comment_id, **kwargs):
    comment = get_object_or_404(WallItem, id=comment_id)

    if not comment.get_editable():
        raise Http404

    if request.user != comment.user:
        message = "Don't touch this!"
        return HttpResponse(
            json.dumps({'status': 'error', 'message': message}))

    data = request.POST.get('comment', False)
    if data:
        comment.body = data
        comment.save()
        return HttpResponse(
            json.dumps({
                'status': 'success',
                'message': comment.body_html,
                'id': comment.id
                }))
    return HttpResponse(
        json.dumps({
            'status': 'success',
            'message': comment.body,
            'id': comment.id
            })
    )

@login_required
def del_item(request, comment_id, **kwargs):
    comment = get_object_or_404(WallItem, id=comment_id)

    if not comment.get_editable():
        raise Http404

    if request.user != comment.user:
        message = "Don't touch this!"
        return HttpResponse(
            json.dumps({'status': 'error', 'message': message}))
    else:
        comment.is_removed = True
        comment.save()
        return HttpResponse(
            json.dumps({'status': 'success', 'message': 'Deleted'}))