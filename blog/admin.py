from django.contrib import admin
from guardian.admin import GuardedModelAdmin
from blog.models import *


class CategoryAdmin(admin.ModelAdmin):
    list_display  = ('title', 'blog', 'slug')
    prepopulated_fields = {'slug': ('title',)}
admin.site.register(Category, CategoryAdmin)

class PostAdmin(GuardedModelAdmin):
    list_display  = ('title', 'publish', 'status', 'is_delete')
    list_filter   = ('publish', 'status')
    search_fields = ('title', 'body')
admin.site.register(Post, PostAdmin)

class BlogAdmin(GuardedModelAdmin):
    list_display = ('title', 'slug', 'owner')
admin.site.register(Blog, BlogAdmin)