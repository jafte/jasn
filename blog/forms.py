from django.forms import ModelForm, Form, ImageField, ModelMultipleChoiceField, CheckboxSelectMultiple
from blog.models import Post, Blog, Category
from django.forms.widgets import HiddenInput, TextInput, Textarea


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = (
            'title',
            'slug',
            'blog',
        )

        widgets = {
            'blog': HiddenInput(),
        }

class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = (
        	'title', 
        	'body', 
        	'tags', 
        	'allow_comments', 
        	'categories', 
    	)
        widgets = {
            'title': TextInput(attrs={'class': 'span12', }),
            'body': Textarea(attrs={'class': 'span12', 'rows': 20, }),
            'tags': TextInput(attrs={'class': 'span12', }),
        }

    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        blog = kwargs['initial'].get('blog', False)
        if not blog:
            post = kwargs.get('instance', False)
            blog = post.blog
        self.fields['categories']= ModelMultipleChoiceField(widget=CheckboxSelectMultiple,queryset=Category.objects.filter(blog=blog), required=False)

class PostDeleteForm(ModelForm):
    class Meta:
        model = Post
        fields = ()

class BlogForm(ModelForm):
    class Meta:
        model = Blog
        fields = (
            'title',
            'description'
        )
        widgets = {
            'title': TextInput(attrs={'class': 'span12', }),
            'description': Textarea(attrs={'class': 'span12', 'rows': 10, }),
        }

class BlogFormAdd(ModelForm):
    class Meta:
        model = Blog
        fields = (
            'title',
            'slug',
            'description'
        )
        widgets = {
            'title': TextInput(attrs={'class': 'span12', }),
            'slug': TextInput(attrs={'class': 'span12', }),
            'description': Textarea(attrs={'class': 'span12', 'rows': 10, }),
        }

class ImageUploadForm(Form):
    image = ImageField()