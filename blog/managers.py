from django.db.models import Manager
import datetime


class PublicManager(Manager):
    """Returns published posts that are not in the future."""

    def published(self):
        return self.get_query_set().filter(status=2, is_delete=False, publish__lte=datetime.datetime.now())

    def unpublished(self):
        return self.get_query_set().filter(status=1, is_delete=False)

    def upcoming(self):
        return self.get_query_set().filter(status=2, is_delete=False, publish__gt=datetime.datetime.now())
