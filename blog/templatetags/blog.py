# -*- coding: utf-8 -*-

import re, random


from django import template
from django.conf import settings
from django.db import models

Blog = models.get_model('blog', 'blog')
Post = models.get_model('blog', 'post')
Category = models.get_model('blog', 'category')
BlogRoll = models.get_model('blog', 'blogroll')

register = template.Library()

class UsersBlogs(template.Node):
    def __init__(self, user, var_name):
        self.user = user
        self.var_name = var_name

    def render(self, context):
        user = template.Variable(self.user).resolve(context)
        blogs = Blog.objects.filter(owner=user, is_delete=False)
        context[self.var_name] = blogs
        return ''
        
@register.tag
def get_user_blogs(parser, token):
    """
    Gets any number of latest posts and stores them in a varable.

    Syntax::

        {% get_user_blogs [user] as [var_name] %}

    Example usage::

        {% get_user_blogs user as latest_post_list %}
    """
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError, "%s tag requires arguments" % token.contents.split()[0]
    m = re.search(r'(.*?) as (\w+)', arg)
    if not m:
        raise template.TemplateSyntaxError, "%s tag had invalid arguments" % tag_name
    format_string, var_name = m.groups()
    return UsersBlogs(format_string, var_name)

class LatestPosts(template.Node):
    def __init__(self, limit, var_name):
        self.limit = int(limit)
        self.var_name = var_name

    def render(self, context):
        posts = Post.objects.published()[:self.limit]
        if posts and (self.limit == 1):
            context[self.var_name] = posts[0]
        else:
            context[self.var_name] = posts
        return ''


@register.tag
def get_latest_posts(parser, token):
    """
    Gets any number of latest posts and stores them in a varable.

    Syntax::

        {% get_latest_posts [limit] as [var_name] %}

    Example usage::

        {% get_latest_posts 10 as latest_post_list %}
    """
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError, "%s tag requires arguments" % token.contents.split()[0]
    m = re.search(r'(.*?) as (\w+)', arg)
    if not m:
        raise template.TemplateSyntaxError, "%s tag had invalid arguments" % tag_name
    format_string, var_name = m.groups()
    return LatestPosts(format_string, var_name)


class BlogCategories(template.Node):
    def __init__(self, var_name):
        self.var_name = var_name

    def render(self, context):
        categories = Category.objects.all()
        context[self.var_name] = categories
        return ''


@register.tag
def get_blog_categories(parser, token):
    """
    Gets all blog categories.

    Syntax::

        {% get_blog_categories as [var_name] %}

    Example usage::

        {% get_blog_categories as category_list %}
    """
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError, "%s tag requires arguments" % token.contents.split()[0]
    m = re.search(r'as (\w+)', arg)
    if not m:
        raise template.TemplateSyntaxError, "%s tag had invalid arguments" % tag_name
    var_name = m.groups()[0]
    return BlogCategories(var_name)


@register.filter
def get_links(value):
    """
    Extracts links from a ``Post`` body and returns a list.

    Template Syntax::

        {{ post.body|markdown:"safe"|get_links }}

    """
    try:
        try:
            from BeautifulSoup import BeautifulSoup
        except ImportError:
            from beautifulsoup import BeautifulSoup
        soup = BeautifulSoup(value)
        return soup.findAll('a')
    except ImportError:
        if settings.DEBUG:
            raise template.TemplateSyntaxError, "Error in 'get_links' filter: BeautifulSoup isn't installed."
    return value


class BlogRolls(template.Node):
    def __init__(self, var_name):
        self.var_name = var_name

    def render(self, context):
        blogrolls = BlogRoll.objects.all()
        context[self.var_name] = blogrolls
        return ''


@register.tag
def get_blogroll(parser, token):
    """
    Gets all blogroll links.

    Syntax::

        {% get_blogroll as [var_name] %}

    Example usage::

        {% get_blogroll as blogroll_list %}
    """
    try:
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError, "%s tag requires arguments" % token.contents.split()[0]
    m = re.search(r'as (\w+)', arg)
    if not m:
        raise template.TemplateSyntaxError, "%s tag had invalid arguments" % tag_name
    var_name = m.groups()[0]
    return BlogRolls(var_name)

@register.filter('read_more')
def read_more(body, absolute_url=False):

    if absolute_url:
        myre = re.compile(ur'\[cut=([^\]]+)\]([^\]]+)', re.UNICODE)
        return myre.sub(u'<p><a class="btn" href="'+str(absolute_url)+u'">читать дальше...</a></p>', body)
    else:
        myre = re.compile(ur'\[cut=([^\]]+)\]', re.UNICODE)
        return myre.sub('', body)

read_more.is_safe = True

@register.filter('hide_text')
def hide_text(body, user):

    if user.is_authenticated():
        myre1 = re.compile(ur'\[hide\]', re.UNICODE)
        myre2 = re.compile(ur'\[/hide\]', re.UNICODE)
        body = myre1.sub('<div class="hide_text alert">', body)
        body = myre2.sub('</div>', body)
    else:
        myre = re.compile(ur'\[hide\]([^\]]+)\[/hide\]', re.UNICODE)
        body = myre.sub(u'<div class="hide_text alert">только для авторизованных пользователей</div>', body)

    return body

hide_text.is_safe = True

@register.filter('roll_text')
def roll_text(body):

    d20 = random.randint(1, 20)
    myre1 = re.compile(ur'\[d20\]', re.UNICODE)
    body = myre1.sub('<b>%s</b>' % d20, body)

    return body

roll_text.is_safe = True