# coding=utf-8
from django.contrib.auth.models import User
from django.utils.hashcompat import sha_constructor
from django.db import models
from django.conf import settings
from django.db.models import permalink
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from blog.managers import PublicManager
from tagging.fields import TagField
from user_groups.models import Group

import datetime, random, markdown, re
from bs4 import BeautifulSoup

from easy_thumbnails.fields import ThumbnailerImageField

from easy_thumbnails.files import get_thumbnailer

from djangoratings.fields import VotingField, FavoriteField

def generate_sha1(string, salt=None):
    if not salt:
        salt = sha_constructor(str(random.random())).hexdigest()[:5]
    hash = sha_constructor(salt+str(string)).hexdigest()

    return (salt, hash)

def make_upload_path(instance, filename):
    extension = filename.split('.')[-1].lower()
    salt, hash = generate_sha1(instance.id)
    return '%(path)s%(hash)s.%(extension)s' % {'path': 'upload/',
                                               'hash': hash[:10],
                                               'extension': extension}

class Blog(models.Model):
    """Blog model."""
    title = models.CharField(_('title'), max_length=100)
    slug = models.SlugField(_('slug'), unique=True)
    is_delete = models.BooleanField(_('is delete'), default=False)
    description = models.TextField(_('description'), blank=True)
    description_html = models.TextField(_('description html'), blank=True, null=True)
    created = models.DateTimeField(_('created'), auto_now_add=True)
    modified = models.DateTimeField(_('modified'), auto_now=True)
    owner = models.ForeignKey(User)
    group = models.ForeignKey(Group, blank=True, null=True, related_name='group_blog')
    favorite = FavoriteField()

    class Meta:
        verbose_name = _('blog')
        verbose_name_plural = _('blogs')
        ordering = ('title',)
        permissions = (
                ('add_post_blog', 'Can add post to blog'),
            )

    def __unicode__(self):
        return u'%s by %s' % (self.title, self.owner)
        
    @permalink
    def get_absolute_url(self):
        return ('blog_detail', None, {'blog_slug': self.slug})
        
    def get_posts_count(self):
        return Post.objects.published().filter(blog=self).count()

    def save(self, *args, **kwargs):
        self.description_html = markdown.markdown(self.description, extensions=['nl2br', ])
        super(Blog, self).save(*args, **kwargs)

class Category(models.Model):
    """Category model."""
    blog = models.ForeignKey(Blog, related_name='blog_categories', help_text=u'Адрес, по которому будет доступна данная категория.')
    title = models.CharField(_('title'), max_length=100)
    slug = models.SlugField(_('slug'))

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')
        unique_together = ("slug", "blog")
        ordering = ('title',)

    def __unicode__(self):
        return u'%s' % (self.title)
        
    @permalink
    def get_absolute_url(self):
        return ('blog_category_detail', None, {'blog_slug': self.blog.slug ,'slug': self.slug})

    def get_posts_count(self):
        return Post.objects.published().filter(categories=self, is_delete = False).count()

class Post(models.Model):
    """Post model."""
    STATUS_CHOICES = (
        (1, _('Draft')),
        (2, _('Public')),
    )
    title = models.CharField(_('title'), max_length=200)
    author = models.ForeignKey(User)
    blog = models.ForeignKey(Blog, related_name='posts')
    body = models.TextField(_('body'), )
    body_html = models.TextField(_('body html'), blank=True, null=True)
    body_anons = models.TextField(_('body anons'), blank=True, null=True)
    body_html_anon = models.TextField(_('body html for anon'), blank=True, null=True)
    body_anons_anon = models.TextField(_('body anons for anon'), blank=True, null=True)
    status = models.IntegerField(_('status'), choices=STATUS_CHOICES, default=2)
    allow_comments = models.BooleanField(_('allow comments'), default=True)
    is_delete = models.BooleanField(_('is delete'), default=False)
    publish = models.DateTimeField(_('publish'), default=datetime.datetime.now)
    created = models.DateTimeField(_('created'), auto_now_add=True)
    modified = models.DateTimeField(_('modified'), auto_now=True)
    categories = models.ManyToManyField(Category, blank=True)
    tags = TagField()
    favorite = FavoriteField()
    rating = VotingField()
    objects = PublicManager()

    class Meta:
        verbose_name = _('post')
        verbose_name_plural = _('posts')
        db_table  = 'blog_posts'
        ordering  = ('-publish',)
        get_latest_by = 'publish'

    def __unicode__(self):
        return u'%s' % self.title

    @permalink
    def get_absolute_url(self):
        return (
            'post_detail', 
            None, 
            {
                'blog_slug': self.blog.slug,
                'pk': self.pk,
            }
        )

    def get_previous_post(self):
        try:
            post = self.get_previous_by_publish(status__gte=2, blog=self.blog, is_delete=False)
        except Post.DoesNotExist:
            return False
        else:
            return post

    def get_next_post(self):
        try:
            post = self.get_next_by_publish(status__gte=2, blog=self.blog, is_delete=False)
        except Post.DoesNotExist:
            return False
        else:
            return post

    def save(self, *args, **kwargs):
        html = self.body
        myre = re.compile(ur'\[cut(=?[^\]]*)\]', re.UNICODE)
        result = myre.split(html)
        
        anons = result[0]
        html = ''.join(result)

        spoiler1 = re.compile(ur'\[spoiler\]', re.UNICODE)
        spoiler2 = re.compile(ur'\[/spoiler\]', re.UNICODE)
        
        anons = spoiler1.sub(u'<span class="splr">', anons)
        anons = spoiler2.sub(u'</span>', anons)

        html = spoiler1.sub(u'<span class="splr">', html)
        html = spoiler2.sub(u'</span>', html)

        self.body_anons = markdown.markdown(anons, extensions=['nl2br', ])
        self.body_html = markdown.markdown(html, extensions=['nl2br', ])

        soup_b = BeautifulSoup(self.body_html, "html.parser")
        imgs_b = soup_b.find_all('img')

        for img in imgs_b:
            if not img['src'].startswith('http://'):
                file_img = img['src'].replace(settings.MEDIA_URL, '', 1)
                try:
                    obj_img = Image.objects.get(file=file_img)
                    if img.parent.name != 'a' and not img['src'].endswith('.gif'):
                        thumb = get_thumbnailer(obj_img.file)['post']
                        img['src'] = thumb.url
                        img.wrap(soup_b.new_tag("a", href=obj_img.file.url))
                except:
                    pass

        self.body_html = soup_b.prettify()

        soup_a = BeautifulSoup(self.body_anons, "html.parser")
        imgs_a = soup_a.find_all('img')

        for img in imgs_a:
            if not img['src'].startswith('http://'):
                file_img = img['src'].replace(settings.MEDIA_URL, '', 1)
                try:
                    obj_img = Image.objects.get(file=file_img)
                    if img.parent.name != 'a' and not img['src'].endswith('.gif'):
                        thumb = get_thumbnailer(obj_img.file)['post']
                        img['src'] = thumb.url
                        img.wrap(soup_a.new_tag("a", href=obj_img.file.url))
                except:
                    pass

        self.body_anons = soup_a.prettify()

        super(Post, self).save(*args, **kwargs)

class Image(models.Model):
    file = ThumbnailerImageField(_('file'), upload_to=make_upload_path,)
    created = models.DateTimeField(_('created'), auto_now_add=True)
    user = models.ForeignKey(User)
    post = models.ForeignKey(Post, blank=True, null=True)
    is_delete = models.BooleanField(_('is delete'), default=False)
    in_use = models.BooleanField(_('in use'), default=False)

    class Meta:
        verbose_name = _('image')
        verbose_name_plural = _('images')
        ordering  = ('-created',)
        get_latest_by = 'created'

    def get_delete_url(self):
        return reverse('blog_del_image', args=[self.id,])

    def __unicode__(self):
        return u'%s' % self.file