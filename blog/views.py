# coding=utf-8
from operator import attrgetter
import re
from django.contrib.contenttypes.models import ContentType
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.core.urlresolvers import reverse

from threadedcomments.models import ThreadedComment

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect

from blog.models import Post, Blog, Category, Image
from blog.forms import PostForm, BlogForm, BlogFormAdd, ImageUploadForm, PostDeleteForm, CategoryForm

from guardian.shortcuts import assign

from easy_thumbnails.files import get_thumbnailer
from pure_pagination.mixins import PaginationMixin

from tagging.models import Tag, TaggedItem

from datetime import datetime

from bs4 import BeautifulSoup

import json, markdown



class BlogListView(PaginationMixin, ListView):
    model = Blog
    context_object_name = 'blogs'
    template_name = 'blog/blog_list.html'
    paginate_by = getattr(settings,'BLOGS_PAGESIZE')

    def get_queryset(self):
        qs = Blog.objects.filter(is_delete=False)
        self.request.breadcrumbs(u'Список блогов', '#')
        return qs

class CreateBlog(CreateView):
    form_class = BlogFormAdd
    template_name = 'blog/form_blog.html'
    succes_url = '/success/'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CreateBlog, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CreateBlog, self).get_context_data(**kwargs)
        context['active'] = 'posts'

        self.request.breadcrumbs(u'Список блогов', reverse('blog_index_all'))
        self.request.breadcrumbs(u'Добавить блог', '#')

        return context

    def form_valid(self, form):
        blog = form.save(commit=False)
        blog.owner = self.request.user
        blog.modified = datetime.now()
        blog.save()
        for perm in settings.ASSIGNED_PERMISSIONS['blog']:
            assign(perm[0], self.request.user, blog)

        return redirect('blog_detail', blog_slug=blog.slug)

class UpdateBlog(UpdateView):
    form_class = BlogForm
    model = Blog
    template_name = 'blog/form_blog.html'
    succes_url = '/success/'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(UpdateBlog, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(UpdateBlog, self).get_context_data(**kwargs)
        blog = get_object_or_404(Blog, slug=self.kwargs.get('slug', False))
        context['blog'] = blog
        context['active'] = 'posts'

        self.request.breadcrumbs(u'Список блогов', reverse('blog_index_all'))
        self.request.breadcrumbs(blog.title, reverse('blog_detail', kwargs={'blog_slug': blog.slug}))
        self.request.breadcrumbs(u'Редактировать блог', '#')

        return context

    def form_valid(self, form):
        blog = form.save(commit=False)
        blog.modified = datetime.now()
        blog.save()
        for perm in settings.ASSIGNED_PERMISSIONS['blog']:
            assign(perm[0], self.request.user, blog)

        return redirect('blog_detail', blog_slug=blog.slug)

class CreatePost(CreateView):
    form_class = PostForm
    template_name = 'blog/form_post.html'
    succes_url = '/success/'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CreatePost, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self, **kwargs):
        kwargs = super(CreatePost, self).get_form_kwargs(**kwargs)
        kwargs['initial']['blog'] = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        if self.request.user.has_perm("add_post_blog", kwargs['initial']['blog']):
            return kwargs
        else:
            raise Http404


    def get_context_data(self, **kwargs):
        context = super(CreatePost, self).get_context_data(**kwargs)
        blog = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        context['blog'] = blog
        context['active'] = 'posts'

        self.request.breadcrumbs(u'Список блогов', reverse('blog_index_all'))
        self.request.breadcrumbs(blog.title, reverse('blog_detail', kwargs={'blog_slug': blog.slug}))
        self.request.breadcrumbs(u'Новая запись', '#')

        return context

    def form_valid(self, form):
        post = form.save(commit=False)
        post.blog = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        post.author = self.request.user
        if ("draft" in self.request.POST):
            post.status = 1
        else:
            post.status = 2
        post.save()
        form.save_m2m()
        for perm in settings.ASSIGNED_PERMISSIONS['post']:
            assign(perm[0], self.request.user, post)

        Image.objects.filter(post=post).update(in_use=False)
        soup = BeautifulSoup(post.body_html, "html.parser")
        imgs = soup.find_all('img')

        for img in imgs:
            if not img['src'].startswith('http://'):
                try:
                    file_img = img['alt'].replace(settings.MEDIA_URL, '', 1)
                    obj_img = Image.objects.get(file=file_img)
                    if obj_img:
                        obj_img.in_use = True
                        obj_img.post = post
                        obj_img.save()

                    else:
                        obj_img = Image(file=file_img, post=post, in_use=True, user=self.request.user)
                        obj_img.save()
                except:
                    pass

        return redirect('post_detail', blog_slug=self.kwargs.get('blog_slug', False), pk=post.pk)

class UpdatePost(UpdateView):
    form_class = PostForm
    model = Post
    template_name = 'blog/form_post.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UpdatePost, self).dispatch(*args, **kwargs)

    def get_object(self):
        object = super(UpdatePost, self).get_object()
        if self.request.user.has_perm("change_post", object):
            return object
        else:
            raise Http404

    def get_context_data(self, **kwargs):
        context = super(UpdatePost, self).get_context_data(**kwargs)
        blog =  get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        object = super(UpdatePost, self).get_object()
        images = Image.objects.filter(post=object, is_delete=False)
        context['blog'] = blog
        context['images'] = images

        self.request.breadcrumbs(u'Список блогов', reverse('blog_index_all'))
        self.request.breadcrumbs(blog.title, reverse('blog_detail', kwargs={'blog_slug': blog.slug}))
        self.request.breadcrumbs(object.title, reverse('post_detail', kwargs={'blog_slug': blog.slug, 'pk': object.pk,}))
        self.request.breadcrumbs(u'Редактирование записи', '#')

        return context

    def form_valid(self, form):
        post = form.save(commit=False)
        if ("draft" in self.request.POST):
            post.status = 1
        else:
            post.status = 2
        post.save()
        form.save_m2m()
        
        Image.objects.filter(post=post).update(in_use=False)
        soup = BeautifulSoup(post.body_html, "html.parser")
        imgs = soup.find_all('img')

        for img in imgs:
            if not img['src'].startswith('http://'):
                try:
                    file_img = img['alt'].replace(settings.MEDIA_URL, '', 1)
                    obj_img = Image.objects.get(file=file_img)
                    if obj_img:
                        obj_img.in_use = True
                        obj_img.post = post
                        obj_img.save()

                    else:
                        obj_img = Image(file=file_img, post=post, in_use=True, user=self.request.user)
                        obj_img.save()
                except:
                    pass

        return redirect('post_detail', blog_slug=self.kwargs.get('blog_slug', False), pk=post.pk)


class PostListView(PaginationMixin, ListView):
    model = Post
    context_object_name = 'post'
    template_name = 'blog/post_list.html'
    paginate_by = getattr(settings,'BLOG_PAGESIZE')

    def get_queryset(self):
        post_type = self.kwargs.get('post_type', False)
        if post_type == "draft":
            qs = Post.objects.unpublished()
        elif post_type == "upcoming":
            qs = Post.objects.upcoming()
        else:
            qs = Post.objects.published()

        if self.kwargs.get('blog_slug', False):
            qs = qs.filter(blog__slug=self.kwargs['blog_slug'])
            blog = get_object_or_404(Blog, slug=self.kwargs['blog_slug'])

            self.request.breadcrumbs(u'Список блогов', reverse('blog_index_all'))
            self.request.breadcrumbs(blog.title, '#')

            if not self.request.user.has_perm("add_post_blog", blog) and (post_type == "draft" or post_type == "upcoming"):
                raise Http404

            if blog.is_delete:
                raise Http404

        else:
            self.request.breadcrumbs(u'Последние записи', '#')

        return qs

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        if self.kwargs.get('blog_slug', False):
            blog = get_object_or_404(Blog, slug=self.kwargs['blog_slug'])
            context['blog'] = blog

        post_type = self.kwargs.get('post_type', False)
        if post_type == "draft":
            context['active_type'] = "draft"
        elif post_type == "upcoming":
            context['active_type'] = "upcoming"
        else:
            context['active_type'] = "public"

        context['active'] = 'posts'
        return context

class PostDetailView(DetailView):
    model = Post
    context_object_name = 'post'
    template_name = 'blog/post_detail.html'

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        post = super(PostDetailView, self).get_object()
        context['blog'] = post.blog
        if self.request.user.is_authenticated():
            images = Image.objects.filter(user=self.request.user, is_delete=False, in_use=False, post=post)
            context['images'] = images
        return context


    def get_object(self):
        object = super(PostDetailView, self).get_object()
        if object.is_delete:
            raise Http404

        self.request.breadcrumbs(u'Список блогов', reverse('blog_index_all'))
        self.request.breadcrumbs(object.blog.title, reverse('blog_detail', kwargs={'blog_slug': object.blog.slug}))
        self.request.breadcrumbs(object.title, '#')

        return object

class PostDelete(UpdateView):
    form_class = PostDeleteForm
    model = Post
    template_name = 'blog/confirm_remove_post.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(PostDelete, self).dispatch(request, *args, **kwargs)

    def get_object(self):
        object = super(PostDelete, self).get_object()
        if self.request.user.has_perm("change_post", object):
            return object
        else:
            raise Http404

    def get_context_data(self, **kwargs):
        context = super(PostDelete, self).get_context_data(**kwargs)
        blog = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        post = super(PostDelete, self).get_object()
        context['blog'] = blog

        self.request.breadcrumbs(u'Список блогов', reverse('blog_index_all'))
        self.request.breadcrumbs(blog.title, reverse('blog_detail', kwargs={'blog_slug': blog.slug}))
        self.request.breadcrumbs(post.title, reverse('post_detail', kwargs={'blog_slug': blog.slug, 'pk': post.pk,}))
        self.request.breadcrumbs(u'Удаление записи', '#')

        return context

    def form_valid(self, form):
        post = form.save(False)
        post.is_delete = True
        post.save()

        comments = ThreadedComment.objects.filter(content_type = ContentType.objects.get_for_model(Post), object_pk = post.pk)
        comments.update(is_removed = True)

        return redirect('blog_detail', blog_slug=self.kwargs.get('blog_slug', False))

class CategoryListView(PaginationMixin, ListView):
    model = Category
    context_object_name = 'category'
    template_name = 'blog/category_list.html'

    def get_queryset(self):
        qs = Category.objects.all()
        if self.kwargs['blog_slug']:
            qs = qs.filter(blog__slug=self.kwargs['blog_slug'])
        return qs

    def get_context_data(self, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        blog = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        context['blog'] = blog
        context['active'] = 'posts'

        self.request.breadcrumbs(u'Список блогов', reverse('blog_index_all'))
        self.request.breadcrumbs(blog.title, reverse('blog_detail', kwargs={'blog_slug': blog.slug}))
        self.request.breadcrumbs(u'Разделы блога', '#')

        return context


class CategoryDetailView(DetailView):
    model = Category
    context_object_name = 'category'
    template_name = 'blog/category_detail.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(**kwargs)
        blog = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        object = super(CategoryDetailView, self).get_object()
        context['object_list'] = object.post_set.published()
        context['blog'] = blog
        context['active'] = 'posts'
        context['page'] = self.kwargs.get('page', 1)

        self.request.breadcrumbs(u'Список блогов', reverse('blog_index_all'))
        self.request.breadcrumbs(blog.title, reverse('blog_detail', kwargs={'blog_slug': blog.slug}))
        self.request.breadcrumbs(u'Разделы блога', reverse('blog_category_list', kwargs={'blog_slug': blog.slug}))
        self.request.breadcrumbs(object.title, "#")

        return context

class CreateCategory(CreateView):
    form_class = CategoryForm
    template_name = 'blog/form_category.html'
    succes_url = '/success/'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CreateCategory, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self, **kwargs):
        kwargs = super(CreateCategory, self).get_form_kwargs(**kwargs)
        kwargs['initial']['blog'] = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        blog = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))

        self.request.breadcrumbs(u'Список блогов', reverse('blog_index_all'))
        self.request.breadcrumbs(blog.title, reverse('blog_detail', kwargs={'blog_slug': blog.slug}))
        self.request.breadcrumbs(u'Разделы блога', reverse('blog_category_list', kwargs={'blog_slug': blog.slug}))
        self.request.breadcrumbs(u"Новый раздел", "#")

        if blog.owner == self.request.user:
            return kwargs
        else:
            raise Http404

    def get_context_data(self, **kwargs):
        context = super(CreateCategory, self).get_context_data(**kwargs)
        context['blog'] = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        context['active'] = 'posts'
        return context

    def form_valid(self, form):
        category = form.save(commit=False)
        category.blog = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        category.save()

        return redirect('blog_category_list', blog_slug=self.kwargs.get('blog_slug', False))

class UpdateCategory(UpdateView):
    form_class = CategoryForm
    model = Category
    template_name = 'blog/form_category.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UpdateCategory, self).dispatch(*args, **kwargs)

    def get_object(self):
        object = super(UpdateCategory, self).get_object()
        blog = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))

        self.request.breadcrumbs(u'Список блогов', reverse('blog_index_all'))
        self.request.breadcrumbs(blog.title, reverse('blog_detail', kwargs={'blog_slug': blog.slug}))
        self.request.breadcrumbs(u'Разделы блога', reverse('blog_category_list', kwargs={'blog_slug': blog.slug}))
        self.request.breadcrumbs(object.title, reverse('blog_category_detail', kwargs={'blog_slug': blog.slug, 'slug': object.slug,}))
        self.request.breadcrumbs(u"Редактировать раздел", "#")


        if blog.owner == self.request.user:
            return object
        else:
            raise Http404

    def get_context_data(self, **kwargs):
        context = super(UpdateCategory, self).get_context_data(**kwargs)
        context['blog'] = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        return context

    def form_valid(self, form):
        category = form.save(commit=False)
        category.blog = get_object_or_404(Blog, slug=self.kwargs.get('blog_slug', False))
        category.save()

        return redirect('blog_category_list', blog_slug=self.kwargs.get('blog_slug', False))


class TagDetailView(DetailView):
    model = Tag
    context_object_name = 'tag'
    template_name = 'blog/tag_detail.html'

    def get_context_data(self, **kwargs):
        context = super(TagDetailView, self).get_context_data(**kwargs)
        object = super(TagDetailView, self).get_object()
        context['object_list'] = TaggedItem.objects.get_by_model(Post,object).filter(status=2, is_delete=False )

        context['page'] = self.kwargs.get('page', 1)

        self.request.breadcrumbs(u'Список тегов', reverse('blog_tag_list'))
        self.request.breadcrumbs(object.name, "#")

        return context



class TagListView(PaginationMixin, ListView):
    model = Tag
    context_object_name = 'tags'
    template_name = 'blog/tag_list.html'

    def get_queryset(self):
        qs = Tag.objects.cloud_for_model(Post)
        return qs

    def get_context_data(self, **kwargs):
        context = super(TagListView, self).get_context_data(**kwargs)

        self.request.breadcrumbs(u'Список тегов', '#')

        return context


@login_required
def edit_comment(request, comment_id, **kwargs):
    comment = get_object_or_404(ThreadedComment, id=comment_id)

    if not comment.get_editable():
        raise Http404

    if request.user != comment.user:
        message = "Don't touch this!"
        return HttpResponse(
            json.dumps({'status': 'error', 'message': message}))

    data = request.POST.get('comment', False)
    if data:
        comment.comment = data
        comment.save()
        return HttpResponse(
            json.dumps({
                'status': 'success',
                'message': comment.comment_html,
                'id': comment.id
            }))
    return HttpResponse(
        json.dumps({
            'status': 'success',
            'message': comment.comment,
            'id': comment.id
        })
    )

@login_required
def del_comment(request, comment_id, **kwargs):
    comment = get_object_or_404(ThreadedComment, id=comment_id)

    if not comment.get_editable():
        raise Http404

    if request.user != comment.user:
        message = "Don't touch this!"
        return HttpResponse(
            json.dumps({'status': 'error', 'message': message}))
    else:
        comment.is_removed = True
        comment.save()
        return HttpResponse(
            json.dumps({'status': 'success', 'message': 'Deleted'}))



@login_required
def up_image(request, post_id=False, **kwargs):
    """Upload all images in request.FILES."""
    if post_id:
        obj = get_object_or_404(Post, id=post_id)
    else:
        obj = None

    files = []
    form = ImageUploadForm(request.POST, request.FILES)
    if form.is_valid():
        for name in request.FILES:
            up_file = request.FILES[name]
            image = Image(file=up_file, post=obj, user=request.user)
            image.save()
            thumb = get_thumbnailer(image.file)['editor']
            files.append({'thumb_url': thumb.url, 'original_url': image.file.url, 'id': image.id, 'delete_link': image.get_delete_url()})

    if files:
        return HttpResponse(
            json.dumps({'status': 'success', 'files': files}))

    message = 'Invalid or no image received.'
    return HttpResponse(
        json.dumps({'status': 'error', 'message': message}))

@login_required
def del_image(request, img_id, **kwargs):
    """Delete uploaded image."""
    obj = get_object_or_404(Image, id=img_id, is_delete=False)
    if obj.user == request.user and obj.in_use == False:
        obj.is_delete = True
        obj.save()
        return HttpResponse(
            json.dumps({'status': 'success'}))
    else:
        if obj.in_use:
            message = 'Image used in post.'
        else:
            message = 'Invalid image.'
        return HttpResponse(
            json.dumps({'status': 'error', 'message': message}))

def comment_posted(request):
    if request.GET['c']:
        comment_id = request.GET['c']
        comment = ThreadedComment.objects.get( pk=comment_id )
        if comment:
            return HttpResponseRedirect( comment.get_absolute_url() )

    return HttpResponseRedirect( "/" )