# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from blog.views import PostListView, PostDetailView, BlogListView, CategoryListView, TagDetailView, TagListView, CreateBlog, \
    CreatePost, UpdateBlog, UpdatePost, CategoryDetailView, PostDelete, up_image, del_image, edit_comment, del_comment, \
    CreateCategory, UpdateCategory

urlpatterns = patterns('',
    url(r'^labels/$',
        TagListView.as_view(),
        name='blog_tag_list'
    ),
    url(r'^labels/(?P<pk>[-\d]+)/$',
        TagDetailView.as_view(),
        name='blog_tag_detail'
    ),
    url(r'^img/add/(?P<post_id>[-\d]+)/$',
        up_image,
        name='blog_up_image_post'
    ),
    url(r'^img/add/$',
        up_image,
        name='blog_up_image'
    ),
    url(r'^img/del/(?P<img_id>[-\d]+)/$',
        del_image,
        name='blog_del_image'
    ),
    url(r'^cmnt/edit/(?P<comment_id>[\d]+)/$',
        edit_comment,
        name='blog_edit_comment'
    ),
    url(r'^cmnt/del/(?P<comment_id>[\d]+)/$',
        del_comment,
        name='blog_del_comment'
    ),

    url(r'^page-(?P<page>\d+)/$',
        PostListView.as_view(),
        name='blog_index_paginated'
    ),
    url(r'^list/$',
        BlogListView.as_view(),
        name='blog_index_all'
    ),
    url(r'^list/page-(?P<page>\d+)/$',
        BlogListView.as_view(),
        name='blog_index_all_paginated'
    ),
    url(r'^add/$',
        CreateBlog.as_view(),
        name='blog_form_add'
    ),
    url(r'^(?P<blog_slug>[-\w]+)/page-(?P<page>\d+)/$',
        PostListView.as_view(),
        name='blog_detail_paginated'
    ),
    url(r'^(?P<blog_slug>[-\w]+)/$',
        PostListView.as_view(),
        name='blog_detail'
    ),
    url(r'^(?P<blog_slug>[-\w]+)/(?P<post_type>(draft|upcoming))/$',
        PostListView.as_view(),
        name='blog_detail_more'
    ),
    url(r'^(?P<blog_slug>[-\w]+)/write/$',
        CreatePost.as_view(),
        name='post_form'
    ),
    url(r'^(?P<slug>[-\w]+)/edit/$',
        UpdateBlog.as_view(),
        name='blog_form_edit'
    ),
    url(r'^(?P<blog_slug>[-\w]+)/edit/(?P<pk>[-\d]+)/$',
        UpdatePost.as_view(),
        name='post_form_id'
    ),
    url(r'^(?P<blog_slug>[-\w]+)/delete/(?P<pk>[-\d]+)/$',
        PostDelete.as_view(),
        name='post_delete'
    ),
    url(r'^(?P<blog_slug>[-\w]+)/p(?P<pk>[-\d]+)/$',
        PostDetailView.as_view(),
        name='post_detail'
    ),
    url (r'^(?P<blog_slug>[-\w]+)/categories/$',
        CategoryListView.as_view(),
        name='blog_category_list'
    ),
    url (r'^(?P<blog_slug>[-\w]+)/categories/add/$',
        CreateCategory.as_view(),
        name='blog_category_add'
    ),
    url(r'^(?P<blog_slug>[-\w]+)/categories/(?P<slug>[-\w]+)/$',
        CategoryDetailView.as_view(),
        name='blog_category_detail'
    ),
    url(r'^(?P<blog_slug>[-\w]+)/categories/(?P<slug>[-\w]+)/page-(?P<page>\d+)/$',
        CategoryDetailView.as_view(),
        name='blog_category_detail'
    ),
    url(r'^(?P<blog_slug>[-\w]+)/categories/(?P<slug>[-\w]+)/edit/$',
        UpdateCategory.as_view(),
        name='blog_category_edit'
    ),
    url(r'^$',
        PostListView.as_view(),
        name='blog_index'
    ),

)
