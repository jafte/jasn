# coding=utf-8
from django.views.generic import ListView
from django.views.generic.simple import direct_to_template
from blog.models import Post
from pure_pagination import PaginationMixin
from threadedcomments.models import ThreadedComment
from wall import WallItem

def index(request):
    comments = ThreadedComment.objects.filter(is_removed=False).order_by('-id')[:20]
    posts = Post.objects.published().filter(rating_score__gt=0)[:10]
    return direct_to_template(
        request,
        'index.html',
        {
        	'comments': comments,
        	'posts': posts,
    	},
    )

class CommentListView(PaginationMixin, ListView):
    model = ThreadedComment
    context_object_name = 'comments'
    template_name = 'comments_list.html'
    paginate_by = 20

    def get_queryset(self):
        qs = ThreadedComment.objects.filter(is_removed=False).order_by('-id')
        return qs

    def get_context_data(self, **kwargs):
        context = super(CommentListView, self).get_context_data(**kwargs)
        context['active'] = 'comments'
        self.request.breadcrumbs(u'Комменатрии', "#")

        return context

class WallListView(PaginationMixin, ListView):
    model = WallItem
    context_object_name = 'wall_item'
    template_name = 'wall_item_list.html'
    paginate_by = 20

    def get_queryset(self):
        qs = WallItem.objects.filter(is_removed=False).order_by('-id')
        return qs

    def get_context_data(self, **kwargs):
        context = super(WallListView, self).get_context_data(**kwargs)
        context['active'] = 'walls'
        self.request.breadcrumbs(u'Записи на стене', "#")

        return context