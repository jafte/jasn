from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, timedelta

class UserActivity(models.Model):
    last_activity_ip = models.IPAddressField()
    last_activity_date = models.DateTimeField(default = datetime(1941, 1, 1))
    user = models.OneToOneField(User, primary_key=True)

    def is_online(self):
        no_active_delta = datetime.now() - self.last_activity_date
        if no_active_delta > timedelta(minutes=21):
            return False
        else:
            return True