from datetime import datetime, timedelta
from lastActivityDate.models import UserActivity

def get_online_users(request):
    fifteen_minutes = datetime.now() - timedelta(minutes=15)
    day = datetime.now() - timedelta(hours=24)
    sql_datetime_15 = datetime.strftime(fifteen_minutes, '%Y-%m-%d %H:%M:%S')
    sql_datetime_day = datetime.strftime(day, '%Y-%m-%d %H:%M:%S')
    return {
        'online_users': UserActivity.objects.filter(last_activity_date__gte=sql_datetime_15, user__is_active__exact=1).order_by('-last_activity_date'),
        'today_online_users': UserActivity.objects.filter(last_activity_date__gte=sql_datetime_day, user__is_active__exact=1).order_by('-last_activity_date'),
    }