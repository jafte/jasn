import markdown

BLOCKQUOTE_SPLITTER = 'blockquote_splitter_paragraph_text'

def makeExtension(configs=None):
    return JasnExtension(configs=configs)

class JasnExtension(markdown.Extension):
    def extendMarkdown(self, md, md_globals):
        md.preprocessors.add('split_blockquotes', SplitBlockquotes(md), '_begin')
        md.postprocessors.add('replace_marker_tags', ReplaceMarkerTags(md), '_end')

class SplitBlockquotes(markdown.preprocessors.Preprocessor):
    def run(self, lines):
        new_lines = []
        for line in lines:
            if line.startswith('>') and not line.endswith('  '):
                new_line = line + '\n\n' + BLOCKQUOTE_SPLITTER + '\n\n'
            else:
                new_line = line
            new_lines.append(new_line)
        return new_lines

class ReplaceMarkerTags(markdown.postprocessors.Postprocessor):
    def run(self, text):
        text = re.sub('<p>' + BLOCKQUOTE_SPLITTER + '</p>', u'', text)
        return text