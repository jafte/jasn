from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from user_groups.decorators import *
from user_groups.models import *
from user_groups.forms import *
from blog.models import Post
from tools.shortcuts import render, redirect

from guardian.shortcuts import assign, remove_perm
from guardian.decorators import permission_required
from django.utils.decorators import method_decorator

def group_list(request, username=None,
        template_name='groups/group_list.html'):
    """
    Returns a group list page.

    Templates: ``groups/group_list.html``
    Context:
        group_list
            Group object list
    """
    group_list = Group.objects.filter(is_active=True)
    if request.user.is_authenticated():
        membership_list = group_list.filter(members__user=request.user)
    else:
        membership_list = []
    return render(request, template_name, {
        'group_list': group_list,
        'membership_list': membership_list
    })


@login_required
def group_create(request, template_name='groups/group_form.html'):
    """
    Returns a group form page.

    Templates: ``groups/group_form.html``
    Context:
        form
            GroupForm object
    """
    if request.method == 'POST':
        form = GroupForm(request.POST, request.FILES)
        if form.is_valid():
            group = form.save(commit=False)
            group.creator = request.user
            group.save()
            creator = GroupMember.objects.create(user=request.user, group=group, status=0)


            return redirect(request, group)
    else:
        form = GroupForm()
    return render(request, template_name, {'form': form})


def group_detail(request, slug, template_name='groups/group_detail.html', page=1):
    """
    Returns a group detail page.

    Templates: ``groups/group_detail.html``
    Context:
        group
            Group object
    """
    group = get_object_or_404(Group, slug=slug, is_active=True)
#    if group.invite_only and not GroupMember.objects.is_member(group, request.user):
#        return redirect(request, reverse('groups:join', kwargs={'slug': group.slug}))
#    TODO: remove to join section.
    return render(request, template_name, {
        'group': group,
        'topic_list': Post.objects.published().filter(blog__in=group.group_blog.all())[:10],
        'member_list': group.members.all(),
        'group_blog_list': group.group_blog.all(),
        'page': page,
    })

@permission_required("group.change_group")
def group_edit(request, slug, template_name='groups/group_form.html'):
    """
    Returns a group form page.

    Templates: ``groups/group_form.html``
    Context:
        form
            GroupForm object
        group
            Group object
    """
    group = get_object_or_404(Group, slug=slug)
    form = GroupForm(instance=group)

    if request.method == 'POST':
        form = GroupForm(request.POST, request.FILES, instance=group)
        if form.is_valid():
            form.save()
            return redirect(request, group)
    return render(request, template_name, {
        'form': form,
        'group': group
    })


@permission_required("group.change_group")
def group_remove(request, slug, template_name='groups/group_remove_confirm.html'):
    """
    Returns a group delete confirmation page.

    Templates: ``groups/group_remove_confirm.html``
    Context:
        group
            Group object
    """
    group = get_object_or_404(Group, slug=slug)
    if request.method == 'POST':
        group.is_active = False
        group.save()
        return redirect(request, reverse('groups:groups'))
    return render(request, template_name, {'group': group})


def group_members(request, slug, template_name='groups/group_members.html'):
    """
    Returns members of a group.

    Templates: ``groups/group_members.html``
    Context:
        group
            Group object
        member_list
            User objects
    """
    group = get_object_or_404(Group, slug=slug, is_active=True)
    member_list = group.members.all()
    return render(request, template_name, {
        'group': group,
        'member_list': member_list
    })


@login_required
def group_join(request, slug, template_name='groups/group_join_confirm.html'):
    """
    Returns a group join confirmation page.

    Templates: ``groups/group_join_confirm.html``
    Context:
        group
            Group object
    """
    group = get_object_or_404(Group, slug=slug, is_active=True)
    if request.method == 'POST':
        membership = GroupMember(group=group, user=request.user)
        membership.save()

        for blog in group.group_blog.all():
            assign('add_post_blog', request.user, blog)

        return redirect(request, group)
    return render(request, template_name, {'group': group})

@login_required
def group_left(request, slug, template_name='groups/group_left_confirm.html'):
    """
    Returns a group join confirmation page.

    Templates: ``groups/group_join_confirm.html``
    Context:
        group
            Group object
    """
    group = get_object_or_404(Group, slug=slug, is_active=True)
    if request.method == 'POST':
        membership = get_object_or_404(GroupMember, group=group, user=request.user)
        membership.delete()

        for blog in group.group_blog.all():
            remove_perm('add_post_blog', request.user, blog)

        return redirect(request, group)
    return render(request, template_name, {'group': group})


@membership_required
def group_invite(request, slug, template_name='groups/group_invite.html'):
    """
    Returns an invite form.
    
    Templates: ``groups/group_invite.html``
    Context:
        form
            GroupInviteForm object
    """
    group = get_object_or_404(Group, slug=slug, is_active=True)
    form = GroupInviteForm(initial={'group': group.pk, 'user': request.user.pk})
    return render(request, template_name, {
        'group': group,
        'form': form
    })
