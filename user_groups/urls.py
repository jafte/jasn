from django.conf.urls.defaults import *


GROUP_URL = r'(?P<slug>[-\w]+)/'
PAGE_URL = r'%spages/(?P<page_slug>[-\w]+)/' % GROUP_URL
PAGE_GROUP_URL = r'%spage-(?P<page>[0-9]+)/' % GROUP_URL

urlpatterns = patterns('user_groups.views.groups',
    url(r'^create/$',                                'group_create',         name='create_group'),
    url(r'^%s$' % PAGE_GROUP_URL,                    'group_detail',         name='group'),
    url(r'^%s$' % GROUP_URL,                         'group_detail',         name='group'),
    url(r'^%sedit/$' % GROUP_URL,                    'group_edit',           name='edit_group'),
    url(r'^%sremove/$' % GROUP_URL,                  'group_remove',         name='remove_group'),
    url(r'^%sjoin/$' % GROUP_URL,                    'group_join',           name='join_group'),
    url(r'^%sleft/$' % GROUP_URL,                    'group_left',           name='left_group'),
    url(r'^%smembers/$' % GROUP_URL,                 'group_members',        name='members_group'),
    url(r'^%sinvite/$' % GROUP_URL,                  'group_invite',         name='invite_group'),
    url(r'^$',                                       'group_list',           name='groups'),
)

# Pages
urlpatterns += patterns('user_groups.views.pages',
    url(r'^%spages/create/$' % GROUP_URL,       'page_create',          name='page_create'),
    url(r'^%s$' % PAGE_URL,                     'page_detail',          name='page'),
    url(r'^%sedit/$' % PAGE_URL,                'page_edit',            name='page_edit'),
    url(r'^%sremove/$' % PAGE_URL,              'page_remove',          name='page_remove'),
    url(r'^%spages/$' % GROUP_URL,              'page_list',            name='pages'),
)