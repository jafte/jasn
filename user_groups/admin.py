from django.contrib import admin
from guardian.admin import GuardedModelAdmin
from user_groups.models import *


class GroupMemberInline(admin.TabularInline):
    raw_id_fields = ('user',)
    model = GroupMember
    fk = 'group'

class GroupPageInline(admin.TabularInline):
    model = GroupPage
    fk = 'group'

class GroupAdmin(GuardedModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    raw_id_fields = ('creator',)
    inlines = (
        GroupPageInline,
    )
admin.site.register(Group, GroupAdmin)

class GroupMemberAdmin(admin.ModelAdmin):
    raw_id_fields = ('user', 'group')
    list_display = ('user', 'group', 'status', 'created')
admin.site.register(GroupMember, GroupMemberAdmin)