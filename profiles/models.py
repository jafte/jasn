from django.db.models import permalink
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

from django.core.cache import cache
from django.conf import settings
from blog.models import Blog, Post
from threadedcomments import ThreadedComment

from userena.models import UserenaBaseProfile

import datetime

class Profile(UserenaBaseProfile):
    """ Default profile """
    GENDER_CHOICES = (
        (1, _('Male')),
        (2, _('Female')),
    )

    user = models.OneToOneField(User,
                                unique=True,
                                verbose_name=_('user'),
                                related_name='profile') 

    gender = models.PositiveSmallIntegerField(_('gender'),
                                              choices=GENDER_CHOICES,
                                              blank=True,
                                              null=True)
    website = models.URLField(_('website'), blank=True, verify_exists=True)
    location =  models.CharField(_('location'), max_length=255, blank=True)
    birth_date = models.DateField(_('birth date'), blank=True, null=True)
    about_me = models.TextField(_('about me'), blank=True)
    
    balance = models.PositiveIntegerField(_('balance'), default=0, blank=True)
    stock = models.PositiveIntegerField(_('stock'), default=0, blank=True)

    @property
    def age(self):
        if not self.birth_date: return False
        else:
            today = datetime.date.today()
            # Raised when birth date is February 29 and the current year is not a
            # leap year.
            try:
                birthday = self.birth_date.replace(year=today.year)
            except ValueError:
                day = today.day - 1 if today.day != 1 else today.day + 2
                birthday = self.birth_date.replace(year=today.year, day=day)
            if birthday > today: return today.year - self.birth_date.year - 1
            else: return today.year - self.birth_date.year

    def last_seen(self):
            return cache.get('seen_%s' % self.user.username)

    def online(self):
        return self.user.useractivity.is_online()

    def get_blogs_count(self):
        return Blog.objects.filter(owner = self.user, is_delete = False).count()

    def get_comments_count(self):
        return ThreadedComment.objects.filter(user=self.user).count()

    def get_posts_count(self):
        return Post.objects.published().filter(author=self.user).count()

    @permalink
    def get_absolute_url(self):
        return (
            'userena_profile_detail',
            None,
            {
                'username': self.user.username,
                }
            )