from django.contrib import admin
from profiles.models import Profile
from guardian.admin import GuardedModelAdmin

class ProfileAdmin(GuardedModelAdmin):
    pass

admin.site.register(Profile, ProfileAdmin)

# admin.site.unregister(Profile)