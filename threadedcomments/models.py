# coding=utf-8
from django.db import models
from django.contrib.comments.models import BaseCommentAbstractModel
from django.contrib.comments.managers import CommentManager
from django.conf import settings
from django.utils import timezone

from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
import markdown, re, random
from djangoratings.fields import VotingField

from datetime import datetime, timedelta
from bs4 import BeautifulSoup
from easy_thumbnails.files import get_thumbnailer

from blog.models import Image

PATH_SEPARATOR = getattr(settings, 'COMMENT_PATH_SEPARATOR', '/')
PATH_DIGITS = getattr(settings, 'COMMENT_PATH_DIGITS', 10)
COMMENT_MAX_LENGTH = getattr(settings,'COMMENT_MAX_LENGTH',3000)

class ThreadedComment(BaseCommentAbstractModel):
    user = models.ForeignKey(User, verbose_name=_('user'), blank=True, null=True, related_name="%(class)s_comments")
    comment = models.TextField(_('comment'), max_length=COMMENT_MAX_LENGTH)
    comment_html = models.TextField(_('comment'), max_length=COMMENT_MAX_LENGTH)
    submit_date = models.DateTimeField(_('date/time submitted'), default=None)
    ip_address = models.IPAddressField(_('IP address'), blank=True, null=True)
    is_public = models.BooleanField(_('is public'), default=True, help_text=_('Uncheck this box to make the comment effectively disappear from the site.'))
    is_removed  = models.BooleanField(_('is removed'), default=False, help_text=_('Check this box if the comment is inappropriate. A "This comment has been removed" message will be displayed instead.'))    
    
    parent = models.ForeignKey('self', null=True, blank=True, default=None,
        related_name='children', verbose_name=_('Parent'))
    last_child = models.ForeignKey('self', null=True, blank=True,
        verbose_name=_('Last child'))
    tree_path = models.TextField(_('Tree path'), editable=False,
        db_index=True)

    rating = VotingField()

    objects = CommentManager()

    def _get_name(self):
        return self.userinfo["name"]
    def _set_name(self, val):
        if self.user_id:
            raise AttributeError(_("This comment was posted by an authenticated "\
                                   "user and thus the name is read-only."))
        self.user_name = val
    name = property(_get_name, _set_name, doc="The name of the user who posted this comment")

    def _get_email(self):
        return self.userinfo["email"]
    def _set_email(self, val):
        if self.user_id:
            raise AttributeError(_("This comment was posted by an authenticated "\
                                   "user and thus the email is read-only."))
        self.user_email = val
    email = property(_get_email, _set_email, doc="The email of the user who posted this comment")

    def get_editable(self):
        week = datetime.now() - timedelta(days=7)
        if self.submit_date >= week:
            return True
        else:
            return False

    def get_absolute_url(self, anchor_pattern="#c%(id)s"):
        return self.content_object.get_absolute_url() + (anchor_pattern % self.__dict__)

    def get_as_text(self):
        """
        Return this comment as plain text.  Useful for emails.
        """
        d = {
            'user': self.user or self.name,
            'date': self.submit_date,
            'comment': self.comment,
            'domain': self.site.domain,
            'url': self.get_absolute_url()
        }
        return _('Posted by %(user)s at %(date)s\n\n%(comment)s\n\nhttp://%(domain)s%(url)s') % d

    def _get_depth(self):
        return len(self.tree_path.split(PATH_SEPARATOR))
    depth = property(_get_depth)

    def _root_id(self):
        return int(self.tree_path.split(PATH_SEPARATOR)[0])
    root_id = property(_root_id)

    def _root_path(self):
        return ThreadedComment.objects.filter(pk__in=self.tree_path.
                                              split(PATH_SEPARATOR)[:-1])
    root_path = property(_root_path)

    def save(self, *args, **kwargs):
        html = self.comment 
        d20 = random.randint(1, 20)

        spoiler1 = re.compile(ur'\[spoiler\]', re.UNICODE)
        spoiler2 = re.compile(ur'\[/spoiler\]', re.UNICODE)
        red20 = re.compile(ur'\[d20\]', re.UNICODE)
        
        html = spoiler1.sub(u'<span class="splr">', html)
        html = spoiler2.sub(u'</span>', html)
        html = red20.sub(u'<b class="roll">%s</b>' % d20, html)

        self.comment_html = markdown.markdown(html, extensions=['nl2br', ])

        soup_a = BeautifulSoup(self.comment_html, "html.parser")
        imgs_a = soup_a.find_all('img')

        for img in imgs_a:
            if not img['src'].startswith('http://'):
                file_img = img['src'].replace(settings.MEDIA_URL, '', 1)
                try:
                    obj_img = Image.objects.get(file=file_img)
                    if img.parent.name != 'a' and not img['src'].endswith('.gif'):
                        thumb = get_thumbnailer(obj_img.file)['post']
                        img['src'] = thumb.url
                        img.wrap(soup_a.new_tag("a", href=obj_img.file.url))
                except:
                    pass

        self.comment_html = soup_a.prettify()

        if self.submit_date is None:
            self.submit_date = timezone.now()
        skip_tree_path = kwargs.pop('skip_tree_path', False)
        super(ThreadedComment, self).save(*args, **kwargs)
        if skip_tree_path:
            return None

        tree_path = unicode(self.pk).zfill(PATH_DIGITS)
        if self.parent:
            tree_path = PATH_SEPARATOR.join((self.parent.tree_path, tree_path))

            self.parent.last_child = self
            ThreadedComment.objects.filter(pk=self.parent_id).update(
                last_child=self)

        self.tree_path = tree_path
        ThreadedComment.objects.filter(pk=self.pk).update(
            tree_path=self.tree_path)

    class Meta(object):
        ordering = ('id',)
        db_table = 'threadedcomments_comment'
        verbose_name = _('Threaded comment')
        verbose_name_plural = _('Threaded comments')
        permissions = [("can_moderate", "Can moderate comments")]

    def __unicode__(self):
        return "%s..." % (self.comment[:50])