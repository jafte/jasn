# -*- coding: utf-8 -*-
import os.path
import sys
import re

PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Adam', 'adam@jasn.ru'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

ugettext = lambda s: s
LANGUAGES = (
    ('ru', ugettext('Russian')),
    ('en', ugettext('English')),
)

TIME_ZONE = 'Europe/Samara'
LANGUAGE_CODE = 'ru-RU'

SITE_ID = 1

USE_I18N = True
USE_L10N = True

STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
STATIC_URL = '/s/'

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')
MEDIA_URL = '/m/'

# Additional locations of static files
STATICFILES_DIRS = (
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

SECRET_KEY = 'lHFoo#(01-3848_DJ939021933334jfnNUuwfdg(@$748)!))#@($*(HJ@HD(h9@HD8'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'userena.middleware.UserenaLocaleMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'lastActivityDate.LastActivityMiddleware.LastActivityMiddleware',
    'tools.middleware.PaginationMiddleware',
    'breadcrumbs.middleware.BreadcrumbsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    "lastActivityDate.context_processors.get_online_users",
    "linkexchange_django.context_processors.linkexchange",
)

ROOT_URLCONF = 'urls'

APPEND_SLASH = True

AUTH_PROFILE_MODULE = 'profiles.Profile'
LOGIN_REDIRECT_URL = '/u/%(username)s/'
USERENA_SIGNIN_REDIRECT_URL = '/u/%(username)s/'
LOGIN_URL = '/u/login/'
LOGOUT_URL = '/u/logout/'

ANONYMOUS_USER_ID = -1

USERENA_MUGSHOT_SIZE = 220
USERENA_MUGSHOT_CROP_TYPE = 'scale'
USERENA_DEFAULT_PRIVACY = 'open'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'templates'),
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.comments',
    'django.contrib.markup',
    'django.contrib.sitemaps',
    'django.contrib.humanize',
    
	'haystack',
	
    'debug_toolbar',
    'django_extensions',
    'easy_thumbnails',
    'guardian',
    'linkexchange_django',
    'pymorphy',
    'tagging',
    'south',
    
    'blog',
    'breadcrumbs',
    'djangoratings',
    'inlines',
    'invitations',
    'lastActivityDate',
    'notification',
    'profiles',
    'pure_pagination',
    'relationships',
    'threadedcomments',
    'tools',
    'user_groups',
    'userena',
    'userena.contrib.umessages',
    'wall',
    'wakawaka'
)

COMMENTS_APP = 'threadedcomments'

NOTIFICATION_QUEUE_ALL = True

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)

PAGINATION_SETTINGS = {
    'PAGE_RANGE_DISPLAYED': 6,
    'MARGIN_PAGES_DISPLAYED': 3,
}

INTERNAL_IPS = (
    '37.112.148.127',
)

THUMBNAIL_ALIASES = {
    '': {
        'small':        {'size': (20, 20), 'crop': True},
        'post':         {'size': (600, 800), 'crop': False},
        'editor':       {'size': (250, 150), 'crop': True},
        'tocomment':    {'size': (50, 50), 'crop': True},
        'group':        {'size': (200, 200), 'crop': True},
        'group_small':  {'size': (75, 75), 'crop': True},
    },
}

PYMORPHY_DICTS = {
    'ru': {
        'dir': os.path.join(PROJECT_ROOT, 'rudict'),
        'backend': 'cdb',
    },
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'KEY_PREFIX': 'jasn',
        }
}

ASSIGNED_PERMISSIONS = {
    'post':
        (('change_post', 'Can change post'),
         ('delete_post', 'Can delete post')),
    'blog':
        (('change_blog', 'Can change blog'),
         ('add_post_blog', 'Can add post to blog'),
         ('delete_blog', 'Can delete blog')),
    }

BLOG_PAGESIZE = 30
BLOGS_PAGESIZE = 20

READ_MORE_TEXT = u'читать дальше...'

WALL_HIDE_REMOVED = "True"

DEFAULT_FROM_EMAIL = 'robot@jasn.ru'

IMPERAVI_CUSTOM_SETTINGS = {
    'lang': 'ru'
}

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(os.path.dirname(__file__), 'whoosh_index'),
    },
}

HAYSTACK_SEARCH_RESULTS_PER_PAGE = 50

BREADCRUMBS_AUTO_HOME = True
BREADCRUMBS_HOME_TITLE = u'Главная'

ALLOWED_HOSTS = "*"

try:
    from local_settings import *
except ImportError:
    pass